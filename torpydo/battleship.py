import random
import os
import colorama
import platform

from colorama import Fore, Back, Style
from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.telemetryclient import TelemetryClient

def print_msg(msg='', end='\n'):
    print(Fore.WHITE + msg + Style.RESET_ALL, end=end)

print_msg("Starting")

myFleet = []
enemyFleet = []


def main():
    TelemetryClient.init()
    TelemetryClient.trackEvent('ApplicationStarted', {'custom_dimensions': {'Technology': 'Python'}})
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet
    # clear the screen
    if(platform.system().lower()=="windows"):
        cmd='cls'
    else:
        cmd='clear'
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print_msg("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print_msg("Enemy fleet destroyed:")
        for ship in enemyFleet:
            if ship.is_destroyed:
                print_msg("- " + ship.name)
        print_msg("Enemy fleet alive:")
        for ship in enemyFleet:
            if not ship.is_destroyed:
                print_msg("- " + ship.name)
        print_msg()
        print_msg("Player, it's your turn.")
        print_msg("You can make a shoot.")
        print_msg("Enter coordinates for your shot: ", end='')
        position = parse_position(input())
        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:
            print_hit()

        print_msg("Yeah ! Nice hit !" if is_hit else "Miss")
        print_msg(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print_msg("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        TelemetryClient.trackEvent('Player_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})

        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print_msg(f"Computer shoot in {str(position)} and {'hit your ship!' if is_hit else 'miss'}")
        TelemetryClient.trackEvent('Computer_ShootPosition', {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if is_hit:
            print_hit()
        print_msg("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

        if not (GameController.is_alive_ships(enemyFleet)):
            end_game()
        if not (GameController.is_alive_ships(myFleet)):
            end_game()    


def print_hit():
    print(Fore.RED + r'''
                    \          .  ./
                  \   .:"";'.:..""   /
                     (M^^.^~~:.'"").
                -   (/  .    . . \ \)  -
                   ((| :. ~ ^  :. .|))
                -   (\- |  \ /  |  /)  -
                     -\  \     /  /-
                       \  \   /  /''' + Style.RESET_ALL)


def end_game():
    print_msg()
    print_msg("GAME OVER")
    raise SystemExit

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()
    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print_msg("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print_msg()
        print_msg(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            print_msg(f"Enter position {i+1} of {ship.size} (i.e A3): ", end='')
            position_input = input()
            ship.add_position(position_input)
            TelemetryClient.trackEvent('Player_PlaceShipPosition', {'custom_dimensions': {'Position': position_input, 'Ship': ship.name, 'PositionInShip': i}})


def initialize_enemyFleet():
    global enemyFleet

    prev_fleet_type_string = ""
    prev_fleet_type = -1
    if os.path.exists("settings.dat"):
        with open("settings.dat", "r") as file:
            prev_fleet_type_string = file.read()
        print(f"STR: {prev_fleet_type_string}")
        if prev_fleet_type_string is not None and len(prev_fleet_type_string) != 0:
            prev_fleet_type = int(prev_fleet_type_string)

    fleet_type = random.randint(0, 3)
    while fleet_type == prev_fleet_type:
        fleet_type = random.randint(0, 3)
    enemyFleet = get_fleet_preset(fleet_type)

    with open("settings.dat", "w") as file:
        file.write(f"{fleet_type}")


def get_fleet_preset(fleet_type: int):
    fleet = GameController.initialize_ships()
    if fleet_type == 0:
        fleet[0].positions.append(Position(Letter.B, 4))
        fleet[0].positions.append(Position(Letter.B, 5))
        fleet[0].positions.append(Position(Letter.B, 6))
        fleet[0].positions.append(Position(Letter.B, 7))
        fleet[0].positions.append(Position(Letter.B, 8))
        fleet[1].positions.append(Position(Letter.E, 6))
        fleet[1].positions.append(Position(Letter.E, 7))
        fleet[1].positions.append(Position(Letter.E, 8))
        fleet[1].positions.append(Position(Letter.E, 9))
        fleet[2].positions.append(Position(Letter.A, 3))
        fleet[2].positions.append(Position(Letter.B, 3))
        fleet[2].positions.append(Position(Letter.C, 3))
        fleet[3].positions.append(Position(Letter.F, 8))
        fleet[3].positions.append(Position(Letter.G, 8))
        fleet[3].positions.append(Position(Letter.H, 8))
        fleet[4].positions.append(Position(Letter.C, 5))
        fleet[4].positions.append(Position(Letter.C, 6))
    elif fleet_type == 1:
        fleet[0].positions.append(Position(Letter.D, 1))
        fleet[0].positions.append(Position(Letter.D, 2))
        fleet[0].positions.append(Position(Letter.D, 3))
        fleet[0].positions.append(Position(Letter.D, 4))
        fleet[0].positions.append(Position(Letter.D, 5))
        fleet[1].positions.append(Position(Letter.G, 3))
        fleet[1].positions.append(Position(Letter.G, 4))
        fleet[1].positions.append(Position(Letter.G, 5))
        fleet[1].positions.append(Position(Letter.G, 6))
        fleet[2].positions.append(Position(Letter.H, 8))
        fleet[2].positions.append(Position(Letter.G, 8))
        fleet[2].positions.append(Position(Letter.F, 8))
        fleet[3].positions.append(Position(Letter.A, 7))
        fleet[3].positions.append(Position(Letter.B, 7))
        fleet[3].positions.append(Position(Letter.C, 7))
        fleet[4].positions.append(Position(Letter.F, 5))
        fleet[4].positions.append(Position(Letter.F, 6))
    elif fleet_type == 2:
        fleet[0].positions.append(Position(Letter.H, 1))
        fleet[0].positions.append(Position(Letter.H, 2))
        fleet[0].positions.append(Position(Letter.H, 3))
        fleet[0].positions.append(Position(Letter.H, 4))
        fleet[0].positions.append(Position(Letter.H, 5))
        fleet[1].positions.append(Position(Letter.A, 7))
        fleet[1].positions.append(Position(Letter.B, 7))
        fleet[1].positions.append(Position(Letter.C, 7))
        fleet[1].positions.append(Position(Letter.D, 7))
        fleet[2].positions.append(Position(Letter.G, 1))
        fleet[2].positions.append(Position(Letter.F, 1))
        fleet[2].positions.append(Position(Letter.E, 1))
        fleet[3].positions.append(Position(Letter.B, 4))
        fleet[3].positions.append(Position(Letter.B, 5))
        fleet[3].positions.append(Position(Letter.B, 6))
        fleet[4].positions.append(Position(Letter.E, 8))
        fleet[4].positions.append(Position(Letter.F, 8))
    elif fleet_type == 3:
        fleet[0].positions.append(Position(Letter.F, 2))
        fleet[0].positions.append(Position(Letter.F, 3))
        fleet[0].positions.append(Position(Letter.F, 4))
        fleet[0].positions.append(Position(Letter.F, 5))
        fleet[0].positions.append(Position(Letter.F, 6))
        fleet[1].positions.append(Position(Letter.H, 7))
        fleet[1].positions.append(Position(Letter.H, 8))
        fleet[1].positions.append(Position(Letter.H, 6))
        fleet[1].positions.append(Position(Letter.H, 5))
        fleet[2].positions.append(Position(Letter.C, 2))
        fleet[2].positions.append(Position(Letter.D, 2))
        fleet[2].positions.append(Position(Letter.E, 2))
        fleet[3].positions.append(Position(Letter.E, 5))
        fleet[3].positions.append(Position(Letter.E, 6))
        fleet[3].positions.append(Position(Letter.E, 7))
        fleet[4].positions.append(Position(Letter.A, 4))
        fleet[4].positions.append(Position(Letter.A, 5))
    return fleet


if __name__ == '__main__':
    main()
