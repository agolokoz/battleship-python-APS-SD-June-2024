#/bin/bash

if [[ ! -d battleship ]]; then
  git clone https://gitlab.com/agolokoz/battleship-python-APS-SD-June-2024.git battleship
fi

cd battleship

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
export PYTHONPATH=.
python3 -m torpydo